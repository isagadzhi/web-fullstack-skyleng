<?php
include("News.php");
$news = new News();
if (isset($_GET['act'])){
	switch ($_GET['act']){
		case "delete" :
			$news->deleteNewsItem($_GET['id']);
			break;
		case "addnews":
			$news->addNews($_GET['title'], $_GET['description'], $_GET['tags']);
			break;
		case "unhide":
			$news->unHideNewsItem($_GET['id']);
			break;
		case "hide":
			$news->hideNewsItem($_GET['id']);
			break;
		default:
			break;
	}
}

$newsList = $news->getNewsList();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">

	<title>News admin</title>

	<!-- Custom styles for this template -->
	<link href="css/blog.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link href="css/dashboard.css" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php">Главная</a>
		</div>
	</div>
</nav>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3 col-md-2 sidebar">
			<ul class="nav nav-sidebar">
				<li class="active"><a href="#">Новости</a></li>
				<li>
					<a data-toggle="modal" data-target=".add-news-modal">Добавить новость</a>
				</li>
			</ul>
		</div>
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<!--			<h1 class="page-header">Dashboard</h1>-->

			<h2 class="sub-header">Список новостей</h2>
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
					<tr>
						<th>#</th>
						<th width="150px">Title</th>
						<th>Description</th>
						<th width="100px">Tags</th>
						<th width="150px">Public Date</th>
						<th width="20px"> Видимость </th>
						<th width="20px"> - </th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($newsList as $item) : ?>
					<tr>
						<td><?=$item->id?></td>
						<td><?=$item->title?></td>
						<td><?=substr($item->description, 0, 300)?>&hellip;</td>
						<td><?=$item->tags?></td>
						<td><?=$item->public_date?></td>
						<td><?=$item->status?></td>
						<td>
							<a href="admin.php?act=delete&id=<?=$item->id?>">удалить</a>
							<a href="admin.php?act=hide&id=<?=$item->id?>">скрыть</a>
							<a href="admin.php?act=unhide&id=<?=$item->id?>">показать</a>
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal fade add-news-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Добавление новости</h4>
			</div>
			<div class="modal-body">
				<form class="form" action="admin.php" method="GET">
					<input type="hidden" name="act" value="addnews">
					<input type="text"  name="title" class="form-control" placeholder="Тема новости"><br>
					<textarea           name="description" class="form-control" rows="3" placeholder="Текст новостиь"></textarea><br>
					<input type="text"  name="tags" class="form-control" placeholder="Тэги. Вводите тэги через запятую">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
					<input type="submit" class="btn btn-primary" value="Добавить">
				</div>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</body>
</html>
