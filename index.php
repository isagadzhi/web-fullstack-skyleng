<?php
include("News.php");
$news = new News();

if (isset($_GET['date'])){
	$date = explode("-", $_GET['date']);
	$month =  [
		'январь' => '01',
		'февраль' => '02',
		'март' => '03',
		'апрель' => '04',
		'май' => '05',
		'июнь' => '06',
		'июль' => '07',
		'август' => '08',
		'сентябрь' => '09',
		'октябрь' => '10',
		'ноябрь' => '11',
		'декабрь' => '12'
	];
	$rdate = $date[0];
	$rdate .= (isset($date[1])) ? "-".$month[$date[1]] : "";
} else {
	$rdate = false;
}
$rtag = (isset($_GET['tag'])) ? $_GET['tag'] : false;
$newsList = $news->getNewsItems5List(0, $rdate, $rtag);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<title>News Blog</title>

	<!-- Custom styles for this template -->
	<link href="css/blog.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<div class="blog-masthead">
	<div class="container">
		<nav class="blog-nav">
			<a class="blog-nav-item active" href="index.php">Новости</a>
			<a class="blog-nav-item" href="admin.php">Админка</a>
		</nav>
	</div>
</div>

<div class="container">
	<div class="blog-header">
		<h1 class="blog-title">News Blog</h1>
		<p class="lead blog-description">Независимая служба новостей.</p>
	</div>
	<?php if(isset($_GET['newsitem'])) : ?>
	<hr>
		<div class="blog-post" itemscope itemtype="http://schema.org/NewsArticle">
			<?php $nowost = $news->getNewsItemById($_GET['newsitem']);?>
			<h3 class="blog-post-title" itemprop="description"><?=$nowost->title?></a></h3>
			<p itemprop="text"><?=$nowost->description?></p>
			<div itemprop="keywords">
				<?php foreach (explode(",", $nowost->tags) as $tag) : ?>
					<span class="label label-primary"><?=$tag?></span>
				<?php endforeach; ?>
			</div>
			<script type="text/javascript">(function() {
					if (window.pluso)if (typeof window.pluso.start == "function") return;
					if (window.ifpluso==undefined) { window.ifpluso = 1;
						var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
						s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
						s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
						var h=d[g]('body')[0];
						h.appendChild(s);
					}})();</script>
			<br>
			<div class="pluso" data-background="transparent" data-options="small,square,line,horizontal,nocounter,theme=01" data-services="vkontakte,facebook,odnoklassniki"></div>
		</div>
		<hr>
	<?php else : ?>
	<div class="row">
		<div class="col-sm-8 blog-main" id="news" data-offset = "0" data-date="<?=$rdate?>" data-tag="<?=$rtag?>">
			<?php foreach ($newsList as $item) : ?>
			<div class="blog-post" itemscope itemtype="http://schema.org/NewsArticle">
				<h3 class="blog-post-title" itemprop="description"><a href="index.php?newsitem=<?=$item->id?>"><?=$item->title?></a></h3>
				<p class="blog-post-meta" itemprope="datePublished"><?=$item->public_date?></p>
				<p itemprop="text"><?= substr($item->description, 0, 200);?></p>
				<div itemprop="keywords">
					<?php foreach (explode(",", $item->tags) as $tag) : ?>
						<a href="index.php?tag=<?=$tag?>"><span class="label label-info"><?=$tag?></span></a>
					<?php endforeach; ?>
				</div>
			</div>
			<hr>
			<?php endforeach; ?>
		</div>

		<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
			<div class="sidebar-module">
				<h4>По датe</h4>
				<ol class="list-unstyled">
					<?php foreach ($news->getDates() as $year=>$mm) :?>
					<li>
						<a href="index.php?date=<?=$year?>"><?=$year?></a>
						<ul>
						<?php foreach ($mm as $moth=>$count) :?>
							<li><a href="index.php?date=<?=$year?>-<?=$moth?>"><?=$moth?> (<?=$count?>)</a></li>
						<?php endforeach; ?>
						</ul>
					</li>
					<?php endforeach; ?>

				</ol>
			</div>
			<div class="sidebar-module">
				<h4>По тегам</h4>
				<ol class="list-unstyled">
					<?php foreach ($news->getTags() as $key=>$value) : ?>
					<li><a href="index.php?tag=<?=$key?>"><?=$key?>(<?=$value?>)</a></li>
					<?php endforeach; ?>
				</ol>
			</div>
		</div><!-- /.blog-sidebar -->

	</div><!-- /.row -->
	<script src="script.js"></script>
	<?php endif; ?>
</div><!-- /.container -->




<footer class="blog-footer">
	<p>Blog template built for <a href="http://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
	<p>
		<a href="#">Back to top</a>
	</p>
</footer>
</body>
</html>
