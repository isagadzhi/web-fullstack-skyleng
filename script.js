var news = $('#news');
var download = false;
var date = news.data('date');
var tag = news.data('tag');

$(window).scroll(function() {
	if (($(window).scrollTop() + $(window).height()) > ($(document).height() - 50) && !download){
		download = true;
		var offset = news.data('offset');
		offset += 5;
		news.data('offset', offset);
		loadNews(offset);
	}
	console.log(news.data('offset'));

});

function loadNews(offset) {
	$.ajax({
		type: 'GET',
		url: 'newslist.php',
		data: 'page=' + offset + '&date=' + date + '&tag=' + tag,
		dataType: "json"
	}).done(function(newsList) {
		console.log(offset);
		news.append(newsList.news);
		if (newsList.finish == 2) {
			$(window).off('scroll');
		}
		download = false;
		console.log('Ответ получен: ', newsList);

		if (newsList.finish) {
			console.log('ОК!)');
		} else {
			console.log('Error!');
		}
	}).fail(function() {
		download = false;
	});
}

