<?php
require_once ("News.php");
$news = new News();
if (isset($_GET['page'])){
	$page = $_GET['page'];
} else {
	$page = 0;
}

if (isset($_GET['date'])){
	$date = explode("-", $_GET['date']);
	$month =  [
		'январь' => '01',
		'февраль' => '02',
		'март' => '03',
		'апрел' => '04',
		'май' => '05',
		'июнь' => '06',
		'июль' => '07',
		'август' => '08',
		'сентябрь' => '09',
		'октябрь' => '10',
		'ноябрь' => '11',
		'декабрь' => '12'
	];
	$date = $date[0]. "-" . $month[$date[1]];
} else {
	$date = false;
}
$rtag = (isset($_GET['tag'])) ? $_GET['tag'] : false;
$newsList = $news->getNewsItems5List($page, $date, $rtag);

$content = "";
foreach ($newsList as $item) {
	$content .= "<div class=\"blog-post\" itemscope itemtype=\"http://schema.org/NewsArticle\">";
	$content .= "<h3 class=\"blog-post-title\" itemprop=\"description\"><a href=\"index.php?newsitem={$item->id}\">{$item->title}</a></h3>";
	$content .= "<p class=\"blog-post-meta\" itemprope=\"datePublished\">{$item->public_date}</p>";
	$description = substr($item->description, 0, 200);
	$content .= "<p itemprop=\"text\">{$description}</p>";
	$content .= "<div itemprop=\"keywords\">";
	foreach (explode(",", $item->tags) as $tag) {
		$content .= "<span class=\"label label-info\">{$tag}</span> &nbsp";
	}
	$content .= "</div></div>";
	$content .= "<hr>";
}
if (empty($content)){
	$newsJson['news'] = $content;
	$newsJson['finish'] = 2;
} else {
	$newsJson['news'] = $content;
	$newsJson['finish'] = 1;
}

echo(json_encode($newsJson));

