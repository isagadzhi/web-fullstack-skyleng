<?php
class News
{
	public function __construct()
	{
		require_once ("DB.php");
		$this->db = new DB;
	}

	public function getNewsList(){
		$sql = "SELECT * FROM `news` ORDER BY `id` DESC";
		return $this->db->query($sql);
	}

	public function getNewsItemById($id){
		$sql = "SELECT * FROM `news`  WHERE id = :id";
		return $this->db->queryOne($sql, [":id" => $id]);
	}

	public function getNewsItems5List($from = 0, $date = false, $tag = false){
		$sql = "SELECT * FROM `news`  WHERE status = 1";
		$sql .= ($date) ? " AND `public_date` REGEXP '{$date}'" : "";
		$sql .= ($tag) ? " AND `tags` REGEXP '{$tag}'" : "";
		$sql .= " ORDER BY `id` DESC LIMIT 5 OFFSET {$from}";
		return $this->db->query($sql);
	}

	public function addNews($title, $description, $tags){
		$sql = "INSERT INTO `news` (`title`, `description`, `tags`) VALUES ('{$title}', '{$description}', '{$tags}')";
		return $this->db->execute($sql);
	}

	public function deleteNewsItem($id){
		$sql = "DELETE FROM `news` WHERE ((`id` = '{$id}'))";
		$this->db->query($sql);
	}

	public function hideNewsItem($id){
		$sql = "UPDATE `news` SET `status` = '0' WHERE `id` = '{$id}'";
		$this->db->query($sql);
	}

	public function unHideNewsItem($id){
		$sql = "UPDATE `news` SET `status` = '1' WHERE `id` = '{$id}'";
		$this->db->query($sql);
	}

	public function getTags(){
		$sql = "SELECT tags FROM `news` WHERE tags != ''";
		$tags = $this->db->query($sql);
		$result = [];
		foreach ($tags as $tag) {
			foreach (explode(",", $tag->tags) as $value) {
				$result[$value] += 1;
			}
		}
		return $result;
	}

	public function getDates(){
		$sql = "SELECT public_date FROM `news` ORDER BY public_date DESC";
		$dates = $this->db->query($sql);
		$month =  ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];
		$result = [];
		foreach ($dates as $date) {
			$date = new DateTime($date->public_date);
			$yy = $date->format("Y");
			$mm = (Int) $date->format("m");
			$dd = $date->format("d");
			$result[$yy][$month[$mm-1]] += 1;
		}
		return $result;
	}
}
