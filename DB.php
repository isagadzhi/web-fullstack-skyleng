<?php
class DB
{
	const DBLOGIN = "web";
	const DBPASSWD = "isa-linux";
	const DB = "web";

	public function __construct()
	{
		$this->pdo = new PDO("mysql:dbname=".self::DB.";host=localhost;charset=utf8", self::DBLOGIN, self::DBPASSWD);
		$this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
	}

	public function query($sql, $params = []){
		$stn = $this->pdo->prepare($sql);
		$stn->execute($params);
		return $stn->fetchAll(PDO::FETCH_CLASS, "stdClass");
	}

	public function queryOne($sql, $params = []){
		$stn = $this->pdo->prepare($sql);
		$stn->execute($params);
		return $stn->fetchAll(PDO::FETCH_CLASS, "stdClass")[0];
	}

	public function execute($sql, $param = []){
		$stn = $this->pdo->prepare($sql);
		$stn->execute($param);
		return $stn->rowCount();
	}

}